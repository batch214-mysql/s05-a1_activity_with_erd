
-- 1. Return the customerName of the customers who are from the Philippines
SELECT * FROM customers WHERE country = "Philippines";

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts".
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. Return the product name and MSRP of the product named "The Titanic".
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4 . Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5. Return the names of customers who have no registered state.
SELECT * FROM customers WHERE state IS NULL;

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve.
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7. Return customer name, country and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8. Return the customer numbers of orders whose comments contain the string "DHL".
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9. Return the product lines whose text description mentions the phrase 'state of the art'.
SELECT productLine FROM productlines Where textDescription LIKE "%state of the art%";

-- 10. Return the countries of customers without duplication.
SELECT DISTINCT country FROM customers;

-- 11. Return the statuses of orders without duplication.
SELECT DISTINCT status FROM orders;

-- 12. Return the customer names and countries of customers whose countries is USA, France OR Canada.
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

-- 13. Return the first name, last name and office's city of employees whose offices are in Tokyo.
-- SELECT firstName, lastName, officeCode FROM employees;
    -- Tokyo officeCode is 5 in office TABLE
        -- needs to join employee TABLE to office TABLE
SELECT employees.firstName, employees.lastName, city FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
WHERE employees.officeCode = 5;

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson".
-- Leslie Thompson employeeNumber is 1166 in employees TABLE
SELECT customerName FROM customers WHERE salesRepEmployeeNumber = 1166;

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
    -- Baane Mini Imports customerNumber is 121 in orders TABLE
        -- needs to join customers TABLE to orders TABLE
            -- customerNumber 121 has orderNumber(s): 10103, 10158, 10309, 10325
                -- needs to join to orderdetails TABLE and products TABLE
SELECT productName, customerName from customers
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode
WHERE customers.customerName = "Baane Mini Imports";

-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country.
    -- need to join employees TABLE to offices TABLE
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;

-- 18. Return the customer's name with a phone number containing "+81".
SELECT customerName from customers where phone LIKE "%+81%";

